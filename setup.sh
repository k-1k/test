#! /bin/env bash

# os
## update
sudo apt update
sudo apt -y upgrade 

## ssh
sudo apt -y install openssh-server
sudo systemctl enable ssh
sudo ufw allow ssh
sudo apt -y install keychain

## net-tools
sudo apt -y install net-tools
sudo apt -y install socat

## git
sudo apt -y install git
git config --global user.email 'kamil.pukala@meteologica.com'
git config --global user.name 'k1k'
git config --global core.editor 'vim'

## tmux
sudo apt -y install tmux

## jq
sudo apt -y install jq

##pgcli
sudo pip install 

## htop
sudo apt -y install htop

## text editors
sudo apt -y install vim
sudo apt -y install meld
snap install code

## dbeaver
sudo snap install dbeaver-ce

##gitkraken
sudo snap install gitkraken --classic

sudo snap install insomnia

## peek
sudo add-apt-repository -y ppa:peek-developers/stable
sudo apt update && sudo apt -y install peek

##guake 
sudo add-apt-repository -y ppa:linuxuprising/guake
sudo apt update && sudo apt -y install guake
## clipboard
### instalar manualmente gnome extension clipboard indicator desde el navegador

# restricted extras
# sudo apt -y install ubuntu-restricted-extras

# preload
sudo apt -y install preload

## battery
sudo apt -y install tlp

## duf 
wget https://github.com/muesli/duf/releases/download/v0.3.1/duf_0.3.1_linux_amd64.deb -o /tmp/duf
dpkg -i /tmp/duf
rm /tmp/duf

## media
sudo apt -y install vlc

## spotify
curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update && sudo apt-get install spotify-client

## screenshots
sudo apt -y install flameshot
# CUSTOM_KEYS=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
# gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings ${CUSTOM_KEYS/]/,\'/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/\']}
# gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name 'screenshots'
# gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command '/usr/bin/flameshot gui'
# gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding '<Ctrl><Alt>ntilde'

# user
## node
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh)"
#source ~/.bashrc
#nvm instal --lts

## ruby
# gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
# curl -sSL https://get.rvm.io | bash -s stable
# source ~/.bashrc
# rvm install ruby-3.0.0
# rvm alias create default ruby-3.0.0
# rvm use default
# rvm gemset create rails7
# gem install rails -v 7.0.1

## www
mkdir -p ~/dwaneti/www

## pass
# sudo apt -y install pass
# git clone git@bitbucket.org:santiagodls/password_store ~/.password-store
# echo 'default-cache-ttl 0' >> '~/.gnupg/gpg-agent.conf'
# echo 'max-cache-ttl 0' >> '~/.gnupg/gpg-agent.conf'
# gpgconf --kill gpg-agent


# config
## gnome
#dconf write /org/gnome/mutter/dynamic-workspaces false
#dconf write /org/gnome/desktop/wm/preferences/num-workspaces 2

## bash + tmux + vim
#sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

#git clone git@github.com:k-1k/config.git /tmp/config
#cp /tmp/config/.zsh* ~/
#cp /tmp/config/.tmux* ~/
#rm -rf /tmp/config
#source ~/.zshrc

# meteologica
git config --global url."ssh://git@gitlab.iss.meteologica.com:2022/meteologica/".insteadOf meteologica:
git config --global merge.tool meld
npm config set registry http://servicios.intranet.meteologica.com:4873/
npm config set workspace-concurrency 12
